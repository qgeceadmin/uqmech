## General
The code repository contains the work of many. This note contains the names of the people who
have contributed in some way (scripts, discussions, snippets of code, etc) to the tools in this
repository. In no order of significance:


Phillip Swann


If you have added/updated scripts or think your name belongs on this list, please add it with note
at the bottom with what your contribution includes. This includes, if you have created a new "project" folder
and want to put up your own tool.



## LSLA
A tool used to analyse leakage and windage losses through a labyrinth seal. Also helps in determining
some geometry parameters not easily determined through analysis (tooth height, tooth thickness, etc.)

Core code - Phillip Swann



## WDC
A tool used in determining the windage (frictional) losses between a stator and rotating face.
Note: this is not for an "annulus", it is designed for a "disc" or "end-effects" of a particular
shaft or rotor.

Core code - Phillip Swann