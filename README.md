# UQMECH

This is a repository for storing code used in the development of the UQMECHXX projects undertaken by the Centre for Energy Futures.

If you are creating new tools or making significant changes to existing tools, please update the "Contributors.md" file with the specifics.
Make sure you commit the changes to this file after you have updated it.

For ensuring you are following the correct procedure for committing your changes, please read the "Developer-Notes.md" file.