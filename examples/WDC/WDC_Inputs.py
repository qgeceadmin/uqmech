# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 13:21:15 2020

@author: uqpswann
"""

"""
General Description: 
This script provides the inputs for the LSLA.py simulation code.

NOTE: When running LSLA.py, you must first select a geommetry, fluid conditions
and numerical settings. Then, run LSLA.py with the inputs script. If the 
simulation returns an error, increase the clearance (geom.c) and the 
relaxation factor (num_set.rf). Increase both until the simulation returns 
a value (i.e. the mass flow rate converges). 

Next, increase/decrease the clearance until the exit pressure is close to that
of the input value (fluid.p_e). Once close, decrease the relaxation factor as 
much as possible (a "good" rf value is ~0.001). Then refine the clearance 
value until satisfied that the exit pressure is within a sufficient error
margin.

-------------------------------------------------------------------------------
Input Classes:
N/A - See LSLA.py

-------------------------------------------------------------------------------
Numerical notes: 
N/A

-------------------------------------------------------------------------------
References: 
[1]    

-------------------------------------------------------------------------------
Project and Description of Inputs: 
UQMECH02 - ASTRI SCO2 Turbine Unit

Design of labyrinth seal used to control the mass flow rate through the DGS and
the cooling zone. This labyrinth seal should maintain a mass flow rate for
a specific deltaP across the seal with the inlet pressure being controlled via
a proportional valve.

-------------------------------------------------------------------------------
Created: 
    Date: 20/12/19
    Author: Phil Swann
    Description: Preliminary
    
-------------------------------------------------------------------------------

"""

geom.r_outer = 0.1
geom.r_inner = 0.03
geom.c = 0.005 


fluid.p_in = 13.5e6
fluid.fluid = 'co2'
fluid.T_in = 743.8

sim_set.speed = 30000
sim_set.m_dot = 0.147

num_set.i_max = 10
num_set.epsilon = 0.00001
num_set.r_divs = 1100
