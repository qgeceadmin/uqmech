"""
General Description: 
This script provides the inputs for the LSLA.py simulation code.

NOTE: When running LSLA.py, you must first select a geommetry, fluid conditions
and numerical settings. Then, run LSLA.py with the inputs script. If the 
simulation returns an error, increase the clearance (geom.c) and the 
relaxation factor (num_set.rf). Increase both until the simulation returns 
a value (i.e. the mass flow rate converges). 

Next, increase/decrease the clearance until the exit pressure is close to that
of the input value (fluid.p_e). Once close, decrease the relaxation factor as 
much as possible (a "good" rf value is ~0.001). Then refine the clearance 
value until satisfied that the exit pressure is within a sufficient error
margin.

-------------------------------------------------------------------------------
Input Classes:
N/A - See LSLA.py

-------------------------------------------------------------------------------
Numerical notes: 
N/A

-------------------------------------------------------------------------------
References: 
[1]    

-------------------------------------------------------------------------------
Project and Description of Inputs: 
UQMECH02 - ASTRI SCO2 Turbine Unit

Design of labyrinth seal used to control the mass flow rate through the DGS and
the cooling zone. This labyrinth seal should maintain a mass flow rate for
a specific deltaP across the seal with the inlet pressure being controlled via
a proportional valve.

-------------------------------------------------------------------------------
Created: 
    Date: 20/12/19
    Author: Phil Swann
    Description: Preliminary
    
-------------------------------------------------------------------------------

"""

geom.l = 0.020
geom.fins = 4
geom.c = 1.0e-4 
geom.D = 0.06

fluid.p_in = 8e6
fluid.p_e = 6.5e6
fluid.fluid = 'co2'
fluid.T_in = 743.79

num_set.i_max = 10000
num_set.epsilon = 0.001
num_set.rf = 0.001

sim_1.plot_mass = False
sim_1.plot_pressure = False
sim_1.plot_temperature = False
sim_1.calc_windage = True
sim_1.speed = 30000 

