"""
General Description: 
This simulation code is used for the design of labyrinth seals.

This script calculates the leakage mass flow rate and pressure distribution 
through a straight through, square/sharp edge tooth labyrinth seal. 

LSLA_Inputs.py is used to supply input parameters for different designs/layouts
to this simulation code.

-------------------------------------------------------------------------------
Input Classes:
Class: CODE - Coding variables
Variables:
    verbosity = used in debugging, 0 - no notes (default), 1 - some notes, 
                2 - many notes
    
Class: NUM_SET - Numerical settings
Variables:
    epsilon =  error to achieve convergence, 0.0001 (default)
    i_max = maximum number of iterations, 10000 (default)
    rf = relaxation factor, 0.1 (default)
    
Class: GEOM - Geometry
Variables:
    l = length available for the labyrinth seal pack (m)
    fins = number of expansions/throttlings 
    t = thickness of each tooth (m)
    p = pitch of the teeth (m)
    h = height of the gap (or length of each tooth) (m)
    c = clearance between rotating surface and tooth tip (m)
-------------------------------------------------------------------------------
Numerical notes: 
Uses compressible flow equations and bulk properties. 

Considers frictionthrough each expansion

Considers real gas dynamics using CoolProp. 

Utilizes the model calculation methodology from ref. [4] (page 78).

-------------------------------------------------------------------------------
References: 
[1]    Cramer et al. 1994 - Fanno Processes in Dense Gases.
[2]    Egli 1935 - The Leakage of Steam through Labyrinth Seals.
[3]    Childs 2005 - Mechanical Design (Section 9.3.1)
[4]    Suryanarayanan 2009 - Masters Thesis - Labyrinth Seal Leakage Equation

-------------------------------------------------------------------------------
Project and Description of Inputs: 
N/A

-------------------------------------------------------------------------------
Created: 
    Date: 19/12/19
    Author: Phil Swann
    Description: Preliminary
    
Updated: 
    Date: 20/12/19
    Author: Phil Swann
    Description: Updated to use same "input" script functionality as the NISI
                 simulation scripts as per PhD thesis.

-------------------------------------------------------------------------------
"""

"""
###############################################################################
FRONT MATTER
###############################################################################
"""

#Modules/Imports
import numpy as np
import scipy as sp
from scipy import interpolate
import sys as sys
import os as os
from getopt import getopt
import shutil
import csv
import CoolProp.CoolProp as cp
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

"""
###############################################################################
Functions
###############################################################################
"""

# Class Definitions
class CODE:
    """
    Class defining code settings.
    """

    def __init__(self):
        """
        Initialise class.
        """
        
        self.verbosity = 0 
        self.plot = False
        
        
class NUM_SETUP:
    """
    Class defining numerical settings.
    """

    def __init__(self):
        """
        Initialise class.
        """
        
        self.verbosity = 0 
        self.epsilon = 0.0001 
        self.i_max = 10 
        self.rf = 0.1
        self.decimals = 5
        
        
class GEOM:
    """
    Class defining geometry.
    """

    def __init__(self):
        """
        Initialise class.
        """
        
        self.l = None
        self.fins = None
        self.t = None
        self.p = None
        self.h = None
        self.c = None
        self.D = None
        
        #Recommended ranges are supplied by Childs (2005) "Mechanical Design"
        self.p_range = np.array([1.8, 2.2, 4., 5., 6., 8.]) #mm
        self.t_range = np.array([0.18, 0.22, 0.28, 0.32, 0.3, 0.4]) #mm
        self.h_range = np.array([1.8, 2.2, 3, 3.5, 4., 5.]) #mm
        
        self.p_range = self.p_range/1000
        self.t_range = self.t_range/1000
        self.h_range = self.h_range/1000
        
        
    def Area(self):
        """
        Calculate the area of the clearance between tooth and rotor.
        """
        
        self.area = np.pi*self.D*self.c
    
    def Pitch(self):
        """
        Calculate the pitch based on the input available length and number
        of fins.
        """
        
        if self.p == None:
            self.p = self.l/self.fins
            print("Pitch = ", self.p*1000, "mm")
            
        else:
            print("Pitch = ", self.p*1000, "mm")
            
    
    def Thickness(self):
        """
        Function for determining thickness of the tooth based on pitch.
        """
        
        if self.t == None:
            if self.p <= self.p_range[0]:
                self.t = self.t_range[0]
                print("Tooth thickness = ", self.t*1000, "mm")
                
            elif self.p >= self.p_range[-1]:
                self.t = self.t_range[-1]
                print("Tooth thickness = ", self.t*1000, "mm")
            
            else:
                t_interp_f = interpolate.interp1d(self.p_range, self.t_range)
                self.t = t_interp_f(self.p)
                print("Tooth thickness = ", self.t*1000, "mm")
        
        
    def Height(self):
        """
        Function for determining height of the tooth based on pitch.
        """
        
        if self.h == None:
            if self.p <= self.p_range[0]:
                self.h = self.h_range[0]
                print("Tooth height = ", self.h*1000, "mm")
                
            elif self.p >= self.p_range[-1]:
                self.h = self.h_range[-1]
                print("Tooth height = ", self.h*1000, "mm")
            
            else:
                h_interp_f = interpolate.interp1d(self.p_range, self.h_range)
                self.h = h_interp_f(self.p)
                print("Tooth height = ", self.h*1000, "mm")
        
    
class FLUID:
    """
    Class defining and handling the fluid properties and lists/data.
    """
        
    def __init__(self, geom):
        """
        Initialise class.
        
        Takes in geom as a class variable. This should be the geom class 
        previously defined.
        """
        
        self.geom = geom
        
        self.p_in = None
        self.p_e = None
        self.fluid = None
        self.T_in = None
        
        
        
    def Set_Up(self):
        """
        This is used to set up the fluid variable storage.
        """
        
        self.p_js = np.zeros(self.geom.fins + 1)
        self.rho_js = np.zeros(self.geom.fins + 1)
        self.cd_js = [0,0]  
        self.psi_js = np.zeros(self.geom.fins) + 1
        self.p_js[-1] = self.p_e
        self.p_js[0] = self.p_in
        
        self.m_dots = []
        self.e_m_dots = []
        
        self.T_js = np.zeros(self.geom.fins + 1)
        
        self.T_js[0] = self.T_in

        self.rho_js[0] = cp.PropsSI('D', 'T', self.T_in, 
                             'P', self.p_in, self.fluid)
        
        
class SIM_SETUP:
    """
    Class defining and handling the simulation functions and variables.
    """
    
    def __init__(self, num_set, geom, fluid):
        """
        Initialise class.
        
        Takes in the num_set, geom and fluid classes. Num_set handles the 
        numerical settings of the simulation.
        
        """
        
        self.num_set = num_set
        self.geom = geom
        self.fluid = fluid
        self.plot_mass = False
        self.plot_pressure = False
        self.plot_temperature = False
        self.speed = None
    
    def Create_Variables(self):
        
        self.c_s = self.geom.c/self.geom.p
        self.w_s = self.geom.t/self.geom.p
        self.w_c = self.geom.t/self.geom.c
        
        self.x = 2.454*self.c_s + 2.268*self.c_s*self.w_s**1.673
        self.y = -1/self.x
        self.z = 1 - 6.5*self.c_s - 8.638*self.c_s*self.w_s
        
        self.R_0 = self.z**self.y
        
        

    def Mass_Flow_Init(self):
        """
        Function handles the calculation of the initial "guess" for the mass
        flow rate. This uses "Martins equation" from Martins (1908) "Labyrinth 
        Packings".
        """
        
        rho = cp.PropsSI('D', 'T', self.fluid.T_in, 'P', \
                         self.fluid.p_in, self.fluid.fluid) # - non-ideal gas 
        self.m_dot_init = self.geom.area * \
                         np.sqrt((1-(self.fluid.p_e/self.fluid.p_in))/
                                 (self.geom.fins-np.log((self.fluid.p_e/\
                                                         self.fluid.p_in)))) *\
                                 np.sqrt(rho * self.fluid.p_in) * \
                                 np.sqrt(1/(1-((self.geom.fins-1)/\
                                               self.geom.fins)*((self.geom.c/\
                                                             self.geom.p)/\
                                                             ((self.geom.c/\
                                                               self.geom.p)+\
                                                             0.02)))) 
                         
        return self.m_dot_init
    

    def Plot_Mass(self):
        fig1, ax1 = plt.subplots()
        ax1.plot(self.fluid.e_m_dots)
        ax1.set_ylabel('Error in m_dot')
        
        fig2, ax1 = plt.subplots()
        ax1.plot(self.fluid.m_dots)
        ax1.set_ylabel("Mass flow rate for each iteration")
        
        plt.show()
        
    def Plot_Pressure(self):
        fig3, ax1 = plt.subplots()
        ax1.plot(self.fluid.p_js)
        ax1.set_ylabel("Pressure distribution")
        plt.show()
    
    def Plot_Temperature(self):
        fig4, ax1 = plt.subplots()
        ax1.plot(self.fluid.T_js)
        ax1.set_ylabel("Temperature distribution")
        plt.show()

    
def main(op_dict):
    """
    Main function.

    Performs the simulation using pre-defined values and LSLA_Inputs.
    
    Also outputs figures and handles what is printed/shown.
    """
    
    print("")
    print("Setting up job file...")
    print("")
    
    # get the job file to be executed
    job_name = op_dict.get("--job", "test")

    # if .py extension is forgotten on job file
    if ".py" not in job_name:
        job_name_check = job_name
        job_name = ''.join([job_name, ".py"])
    else:
        # strip .py extension from jobName
        job_name_check = job_name.replace('.py', '')

    # make sure that the desired job file actually exists
    if job_name not in os.listdir('.'):
        local_directory = os.path.dirname(os.path.realpath(__file__))
        
        # raise an error if the job file name inputted doesn't exist
        raise code_error_1("No job file {0} found in directory: {1}"
                      .format(job_name, local_directory))
    
    # get the output file name/path (if it exists)    
    out_file = op_dict.get("--out_file", "none")
        
    print("\n")
    print("Initializing classes...")
    print("\n")
    
    # initialise the class
    code = CODE()
    geom = GEOM()
    num_set = NUM_SETUP()
    fluid = FLUID(geom)
    
    sim_1 = SIM_SETUP(num_set, geom, fluid)
    
    # set verbosity (can be overwritten from jobfile)
    code.verbosity = float(op_dict.get("--verbosity", 0))

    # execute the job file, this creates and sets all the variables from the 
    # job file
    exec(open(job_name).read(), globals(), locals())
    
    
    geom.Pitch()
    geom.Thickness()
    geom.Height()
    geom.Area()
    
    fluid.Set_Up()
    
    sim_1.Create_Variables()
    
    print("C_S = ", sim_1.c_s)
    print("W_S = ", sim_1.w_s)
    print("W_C = ", sim_1.w_c)
    
    m_dot = sim_1.Mass_Flow_Init()

    sim_1.fluid.m_dots.append(m_dot)
    
    mu = cp.PropsSI('V', 'T', sim_1.fluid.T_in, 'P', sim_1.fluid.p_in, \
                    sim_1.fluid.fluid)
    
    i = 0
    
    while i <= sim_1.num_set.i_max:
        
        print("i = ", i)
        
        Re = m_dot/(np.pi*sim_1.geom.D*mu)
        
        gamma = sim_1.z*(Re + sim_1.R_0)**sim_1.x
            
        sim_1.fluid.cd_js[0] = (0.7757 - (0.002051*sim_1.w_c))/((1 + \
                         (44.86*sim_1.w_c/Re))**0.2157)
        
        sim_1.fluid.cd_js[1] = sim_1.fluid.cd_js[0]*0.925*gamma**0.861
    
        sim_1.fluid.p_js[1] = sim_1.fluid.p_js[0] - (((m_dot/\
                        (sim_1.fluid.psi_js[0]*sim_1.fluid.cd_js[0]*\
                         sim_1.geom.area))**2)/
                        (2*sim_1.fluid.rho_js[0]))
        
        sim_1.fluid.psi_js[0] = 0.558*(sim_1.fluid.p_js[1]/\
                          sim_1.fluid.p_js[0]) + 0.442
        
        
        for j in range(0, sim_1.geom.fins):
            
            
            sim_1.fluid.rho_js[j] = cp.PropsSI('D', 'T', sim_1.fluid.T_js[j], 
                             'P', sim_1.fluid.p_js[j], sim_1.fluid.fluid)
            
            sim_1.fluid.p_js[j+1] = sim_1.fluid.p_js[j] - (((m_dot/\
                            (sim_1.fluid.psi_js[j]*sim_1.fluid.cd_js[1]*\
                             sim_1.geom.area))**2)/(2*sim_1.fluid.rho_js[j]))
            
            sim_1.fluid.T_js[j+1] = cp.PropsSI('T', 'D', sim_1.fluid.rho_js[j], 
                             'P', sim_1.fluid.p_js[j+1], sim_1.fluid.fluid)
            
            sim_1.fluid.rho_js[j+1] = cp.PropsSI('D', 'T',
                              sim_1.fluid.T_js[j+1], 'P', 
                              sim_1.fluid.p_js[j+1], sim_1.fluid.fluid)
            
            
            sim_1.fluid.psi_js[j] = 0.558*(sim_1.fluid.p_js[j+1]/\
                          sim_1.fluid.p_js[j]) + 0.442
            
        m_dot1 = sim_1.fluid.psi_js[-1] * sim_1.fluid.cd_js[1] * \
                sim_1.geom.area * np.sqrt(2. * sim_1.fluid.rho_js[-2] * \
                (sim_1.fluid.p_js[-2] - sim_1.fluid.p_js[-1]))
                    

        sim_1.fluid.m_dots.append(m_dot1)
        
        e_m_dot = np.abs((m_dot1 - m_dot)/m_dot)
        
        sim_1.fluid.e_m_dots.append(e_m_dot)
        
        print("Pressure exit = ", sim_1.fluid.p_js[-1]/1e6," MPa")
        
        if e_m_dot < sim_1.num_set.epsilon:
            print("\n")
            print("The mass flow rate error reached the set criterion")
            print("\n")
            print("The mass flow rate is  {0:0.7f} +/- {1:0.7f} kg/s".format(\
                  m_dot1, e_m_dot))
            print("\n")
            print("The exit pressure is {0:0.4f} MPa".format(
                    sim_1.fluid.p_js[-1]/1e6))
            print("\n")
            print("The exit temperature is {0:0.4f} degC".format(
                    sim_1.fluid.T_js[-1]-273.15))
            
            break
    
        else:
              
            i += 1
            
            m_dot = m_dot + sim_1.num_set.rf * (m_dot1 - m_dot)
            
            
            continue
        
    else:
        print("\n")
        print("CONVERGANCE NOT REACHED")
        print("\n")
        
        print("\n")
        print("The mass flow rate is {0:0.7f} +/- {1:0.7f} kg/s".format(\
              m_dot1, e_m_dot))
        print("\n")
    
    if sim_1.plot_mass:
        sim_1.Plot_Mass()
        
    if sim_1.plot_pressure:
        sim_1.Plot_Pressure()
        
    if sim_1.plot_temperature:
        sim_1.Plot_Temperature()
    
    # if an output file is wanted, create it and set it up to be written to
    if out_file is not "none":
        print("\n")
        print("Creating output file")
        print("\n")
        
        # if there is a file already, set the extension to .old so that it 
        # doesn't get over-written
        if os.path.isfile(out_file):
            print("Output file already exists.")
            f_n_temp = out_file+".old"
            shutil.copyfile(out_file, f_n_temp)
            print("Existing file has been copied to: ", f_n_temp)

        # then write the header to the file
        with open(out_file, 'w') as output_csv:
            output_writer = csv.writer(output_csv)
            output_writer.writerow([" "])
            output_writer.writerow(["LSLA output header."])
            output_writer.writerow([" "])
    
    
    if sim_1.calc_windage:
        if sim_1.speed:
            omega = sim_1.speed*np.pi/30
            
            
            def Find_Cd(dens, visc, clearance):
                reynolds = (geom.D/2) * clearance * omega * dens / visc
                
                def CdFind(Cd):
                    return 2.04 + (1.768 * np.log(reynolds * np.sqrt(Cd))) - \
                (1 / np.sqrt(Cd))
                
                coeffD = fsolve(CdFind, 0.001)
                return coeffD

            def WindageEst(dens, coeffD, len_ = 1):
                #windage estimation function. Returns windage power per unit length. 
                #Vrancik 1968
                
                windage = np.pi * coeffD * dens * (sim_1.geom.D/2)**4.0 * omega**3.0 * len_
                return windage
            
            fluid_d_cd = cp.PropsSI("D", "P", sim_1.fluid.p_in, "T", 
                                    sim_1.fluid.T_in, sim_1.fluid.fluid)
            fluid_v_cd = cp.PropsSI("V", "P", sim_1.fluid.p_in, "T", 
                                    sim_1.fluid.T_in, sim_1.fluid.fluid)
            c_d_expansion = Find_Cd(fluid_d_cd, fluid_v_cd, sim_1.geom.h)
            c_d_tooth = Find_Cd(fluid_d_cd, fluid_v_cd, sim_1.geom.c)
            
            total_windage = 0
            
            for j in range(sim_1.geom.fins):
                fluid_d_w = cp.PropsSI("D", "P", sim_1.fluid.p_js[j], "T", 
                                       sim_1.fluid.T_js[j], sim_1.fluid.fluid)
                
                l_expansion = sim_1.geom.p - sim_1.geom.t
                
                #add windage from expansion section
                total_windage += WindageEst(fluid_d_w, c_d_expansion, 
                                            l_expansion)
                
                #add windage from tooth (restriction) section
                total_windage += WindageEst(fluid_d_w, c_d_tooth, 
                                            sim_1.geom.t)
                

            print("")
            print("Total windage over the labyrinth seal = {0:.4f} W\
                  ".format(total_windage[0]))
            print("")
            
        else:
            print("")
            print("You need to enter the rotational speed of the shaft.")
            print("sim_1.speed")
            print("")
    
    
    # return false at the end of the main function       
    return 0

short_opts = ""
long_opts = ["help", "job=", "verbosity=", "out_file="]


def Instructions():
    """Print usage instructions."""
    print("")
    print("Usage: LSLA.py [--help] [--job=<jobFileName>] \
          [--out_file=<outputFileName>] [--verbosity=0 (1 or 2)]")
    return 0


class code_error_1(Exception):
    """Raise error message."""

    def __init__(self, value):
        """Initialise class."""
        self.value = value

    def __str__(self):
        """Set string."""
        return repr(self.value)


if __name__ == "__main__":
    user_opt = getopt(sys.argv[1:], short_opts, long_opts)
    op_dict = dict(user_opt[0])

    if len(user_opt[0]) == 0 or "--help" in op_dict:
        Instructions()
        sys.exit(1)

    # execute the code
    try:
        main(op_dict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except code_error_1 as e:
        print("\n \n")
        print("This run has gone bad.")
        print(e.value)
        print("\n \n")
        sys.exit(1)
