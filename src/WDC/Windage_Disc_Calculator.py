"""
General Description: 
This simulation code is used for the design of labyrinth seals.

This script calculates the leakage mass flow rate and pressure distribution 
through a straight through, square/sharp edge tooth labyrinth seal. 

LSLA_Inputs.py is used to supply input parameters for different designs/layouts
to this simulation code.

-------------------------------------------------------------------------------
Input Classes:
Class: CODE - Coding variables
Variables:
    verbosity = used in debugging, 0 - no notes (default), 1 - some notes, 
                2 - many notes
    
Class: NUM_SET - Numerical settings
Variables:
    epsilon =  error to achieve convergence, 0.0001 (default)
    i_max = maximum number of iterations, 10000 (default)
    rf = relaxation factor, 0.1 (default)
    
Class: GEOM - Geometry
Variables:
    l = length available for the labyrinth seal pack (m)
    fins = number of expansions/throttlings 
    t = thickness of each tooth (m)
    p = pitch of the teeth (m)
    h = height of the gap (or length of each tooth) (m)
    c = clearance between rotating surface and tooth tip (m)
-------------------------------------------------------------------------------
Numerical notes: 
Uses compressible flow equations and bulk properties. 

Considers frictionthrough each expansion

Considers real gas dynamics using CoolProp. 

Utilizes the model calculation methodology from ref. [4] (page 78).

-------------------------------------------------------------------------------
References: 
[1]    Cramer et al. 1994 - Fanno Processes in Dense Gases.
[2]    Egli 1935 - The Leakage of Steam through Labyrinth Seals.
[3]    Childs 2005 - Mechanical Design (Section 9.3.1)
[4]    Suryanarayanan 2009 - Masters Thesis - Labyrinth Seal Leakage Equation

-------------------------------------------------------------------------------
Project and Description of Inputs: 
N/A

-------------------------------------------------------------------------------
Created: 
    Date: 19/12/19
    Author: Phil Swann
    Description: Preliminary
    
Updated: 
    Date: 20/12/19
    Author: Phil Swann
    Description: Updated to use same "input" script functionality as the NISI
                 simulation scripts as per PhD thesis.

-------------------------------------------------------------------------------
"""

#Modules/Imports
import numpy as np
import scipy as sp
from scipy import interpolate
import sys as sys
import os as os
from getopt import getopt
import shutil
import csv
import CoolProp.CoolProp as cp
import matplotlib.pyplot as plt

"""
###############################################################################
Functions
###############################################################################
"""

# Class Definitions
class CODE:
    """
    Class defining code settings.
    """

    def __init__(self):
        """
        Initialise class.
        """
        
        self.verbosity = 0 
        self.plot = False
        
class GEOM:
    """
    Class defining code settings.
    """

    def __init__(self):
        """
        Initialise class.
        """
        
        self.r_outer = None
        self.r_inner = None
        self.c = None
        
        
class NUM_SET:
    """
    Class defining code settings.
    """

    def __init__(self):
        """
        Initialise class.
        """
        
        self.i_max = None
        self.epsilon = None
        self.r_divs = None
        
class SIM_SET:
    """
    Class defining code settings.
    """

    def __init__(self):
        """
        Initialise class.
        """
        
        self.speed = None 
        self.m_dot = None
        
class FLUID:
    """
    Class defining code settings.
    """

    def __init__(self):
        """
        Initialise class.
        """
        
        self.p_in = None 
        self.T_in = None
        self.fluid = None
        
class SIMULATION:
    """
    Class defining code settings.
    """

    def __init__(self, geom, fluid, num_set, sim_set):
        """
        Initialise class.
        """
        
        self.geom = geom
        self.fluid = fluid
        self.num_set = num_set
        self.sim_set = sim_set
        
    def Setup_Geom(self):
        
        self.rs = np.linspace(self.geom.r_inner, self.geom.r_outer, self.num_set.r_divs)
        
        
    def Setup_Sim(self):
        
        self.omega = np.pi*self.sim_set.speed/30.
        self.Ts = [self.fluid.T_in]
        self.rhos = [cp.PropsSI('D', 'T', self.fluid.T_in, 'P', 
                                self.fluid.p_in, self.fluid.fluid)]
        self.mus = [cp.PropsSI('V', 'T', self.fluid.T_in, 'P', 
                                self.fluid.p_in, self.fluid.fluid)]
        self.pws = [0.]
        self.cps = [cp.PropsSI('C', 'T', self.fluid.T_in, 'P', 
                                self.fluid.p_in, self.fluid.fluid)]
        
    def Power_Loss(self, radius, mu, rho):
        
        pw_rot = 0.0311*(self.geom.c/radius)**(-1/4.)*mu**(1/4.)*\
        rho**(3/4.)*self.omega**(11/4.)*radius**(9/2.)
        
        if self.sim_set.m_dot:
            K_0 = 0.5 # - for "merged" boundary layers. it is the ratio of 
                    # fluid/rotor angular velocity
            pw_flow = K_0 * self.sim_set.m_dot * self.omega**2 * radius**2
        
        else:
            pw_flow = 0.0
        
        return pw_rot + pw_flow
    
    def Delta_T(self, cp, Q):
        
        delta_T = Q/(self.sim_set.m_dot * cp)
        
        return delta_T
        
        
def main(op_dict):
    """
    Main function.

    Performs the simulation using pre-defined values and LSLA_Inputs.
    
    Also outputs figures and handles what is printed/shown.
    """
    
    print("")
    print("Setting up job file...")
    print("")
    
    # get the job file to be executed
    job_name = op_dict.get("--job", "test")

    # if .py extension is forgotten on job file
    if ".py" not in job_name:
        job_name_check = job_name
        job_name = ''.join([job_name, ".py"])
    else:
        # strip .py extension from jobName
        job_name_check = job_name.replace('.py', '')

    # make sure that the desired job file actually exists
    if job_name not in os.listdir('.'):
        local_directory = os.path.dirname(os.path.realpath(__file__))
        
        # raise an error if the job file name inputted doesn't exist
        raise code_error_1("No job file {0} found in directory: {1}"
                      .format(job_name, local_directory))
    
    # get the output file name/path (if it exists)    
    out_file = op_dict.get("--out_file", "none")
        
    print("\n")
    print("Initializing classes...")
    print("\n")
    
    # initialise the class
    code = CODE()
    geom = GEOM()
    fluid = FLUID()
    num_set = NUM_SET()
    sim_set = SIM_SET()
    
 
    # set verbosity (can be overwritten from jobfile)
    code.verbosity = float(op_dict.get("--verbosity", 0))

    # execute the job file, this creates and sets all the variables from the 
    # job file
    exec(open(job_name).read(), globals(), locals())
    
    
    print("\n")
    print("Beginning simulation...")
    print("\n")
    
    sim_1 = SIMULATION(geom, fluid, num_set, sim_set)
    
    sim_1.Setup_Geom()
    sim_1.Setup_Sim()
    
    
  # if an output file is wanted, create it and set it up to be written to
    if out_file is not "none":
        print("\n")
        print("Creating output file")
        print("\n")
        
        # if there is a file already, set the extension to .old so that it 
        # doesn't get over-written
        if os.path.isfile(out_file):
            print("Output file already exists.")
            f_n_temp = out_file+".old"
            shutil.copyfile(out_file, f_n_temp)
            print("Existing file has been copied to: ", f_n_temp)

        # then write the header to the file
        with open(out_file, 'w') as output_csv:
            output_writer = csv.writer(output_csv)
            output_writer.writerow([" "])
            output_writer.writerow(["LSLA output header."])
            output_writer.writerow([" "])
    
    
    pws_i = []
    Ts_i = []
    i = 0
    while i <= sim_1.num_set.i_max:
#        print(len(sim_1.pws))
        for j, r in enumerate(sim_1.rs):
            
            if i == 0:
                pw = sim_1.Power_Loss(r, sim_1.mus[j], sim_1.rhos[j])
                sim_1.pws.append(pw)
                
                T_new = sim_1.Delta_T(sim_1.cps[j], pw) + sim_1.Ts[j]
                sim_1.Ts.append(T_new)
                
                sim_1.rhos.append(cp.PropsSI('D', 'T', sim_1.Ts[j+1], 'P', 
                                sim_1.fluid.p_in, sim_1.fluid.fluid))
                sim_1.cps.append(cp.PropsSI('C', 'T', sim_1.Ts[j+1], 'P', 
                                sim_1.fluid.p_in, sim_1.fluid.fluid))
                sim_1.mus.append(cp.PropsSI('V', 'T', sim_1.Ts[j+1], 'P', 
                                sim_1.fluid.p_in, sim_1.fluid.fluid))
            else:
                pw = sim_1.Power_Loss(r, sim_1.mus[j], sim_1.rhos[j])
                
                sim_1.pws[j+1] = pw
                
                T_new = sim_1.Delta_T(sim_1.cps[j], pw) + sim_1.Ts[j]
                sim_1.Ts[j+1] = T_new
            
                sim_1.rhos[j+1] = cp.PropsSI('D', 'T', sim_1.Ts[j+1], 'P', 
                                sim_1.fluid.p_in, sim_1.fluid.fluid)
                sim_1.cps[j+1] = cp.PropsSI('C', 'T', sim_1.Ts[j+1], 'P', 
                                sim_1.fluid.p_in, sim_1.fluid.fluid)
                sim_1.mus[j+1] = cp.PropsSI('V', 'T', sim_1.Ts[j+1], 'P', 
                                sim_1.fluid.p_in, sim_1.fluid.fluid)
                
        pws_i.append(sim_1.pws)       
        Ts_i.append(sim_1.Ts)

        i += 1
        
    
    plt.plot(sim_1.rs, sim_1.pws[1:])
    plt.show()
    
    print("")
    print("Calculating total windage...")
    print("")
    
    total_windage = 0.
    for i in range(1, len(sim_1.rs)):
        total_windage += sim_1.pws[i] - sim_1.pws[i-1]
    
    print("Total windage = ", total_windage)
    print("")
    
    
    # return false at the end of the main function       
    return 0

short_opts = ""
long_opts = ["help", "job=", "verbosity=", "out_file="]


def Instructions():
    """Print usage instructions."""
    print("")
    print("Usage: LSLA.py [--help] [--job=<jobFileName>] \
          [--out_file=<outputFileName>] [--verbosity=0 (1 or 2)]")
    return 0


class code_error_1(Exception):
    """Raise error message."""

    def __init__(self, value):
        """Initialise class."""
        self.value = value

    def __str__(self):
        """Set string."""
        return repr(self.value)


if __name__ == "__main__":
    user_opt = getopt(sys.argv[1:], short_opts, long_opts)
    op_dict = dict(user_opt[0])

    if len(user_opt[0]) == 0 or "--help" in op_dict:
        Instructions()
        sys.exit(1)

    # execute the code
    try:
        main(op_dict)
        print("\n \n")
        print("SUCCESS.")
        print("\n \n")

    except code_error_1 as e:
        print("\n \n")
        print("This run has gone bad.")
        print(e.value)
        print("\n \n")
        sys.exit(1)

